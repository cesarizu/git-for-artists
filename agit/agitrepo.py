# encoding: UTF-8

import collections
from dataclasses import dataclass
from enum import Enum
import json
import logging
import os
import pathlib
import re
from urllib.parse import urlparse

import git

GIT_FILES = ['.gitattributes', '.gitignore']


def display_name(filename):
    """Adds a trailing slash for directories"""
    return ('%s/' % filename) if filename and filename[-1] != '/' and os.path.isdir(filename) else filename


def match_path(base_path, path, recursive=True):
    """Return true if the path would be matched by the base_path"""
    base_path = re.sub(r'^\./|^\.$', '', base_path)

    if base_path.endswith('/') or base_path == '':
        if recursive:
            return path.startswith(base_path)
        else:
            filename = re.sub(r'.*/([^/]*)', r'\1', path)
            return path.startswith(base_path) and '%s%s' % (base_path, filename) == path
    else:
        return base_path == path


class FileStatus(Enum):
    """The status of a file in the working copy"""
    UNMODIFIED = ' '
    MODIFIED = 'M'
    DELETED = 'D'
    UNTRACKED = '?'
    NOT_CHECKED_OUT = 'O'


class LockStatus(Enum):
    """The lock status of a file"""
    UNLOCKED = 'U'
    LOCKED = 'L'
    LOCKED_OTHER = 'O'


@dataclass
class Status:
    filename: str = ''
    file_status: FileStatus = FileStatus.UNMODIFIED
    lock_status: LockStatus = LockStatus.UNLOCKED

    def __lt__(self, other):
        return self.filename < other.filename


class AGitRepo:
    """Git for artists:

    An easy way of using git for asset-heavy repositories with a simplified workflow.
    """

    def __init__(self, repo_path=None):
        """
        Initializes the AGit object.

        :param repo_path: Is a relative path to the repository. If None then it can only be used for listing remote
                          branches with the `branches` method.
        """
        if repo_path:
            self.repo_path = os.path.abspath(repo_path)
        else:
            self.repo_path = None

        if self.repo_path and os.path.isdir(os.path.join(self.repo_path, '.git')):
            self.git_repo = git.Repo(self.repo_path)
        else:
            self.git_repo = None

    def __create_empty(self):
        """Creates a repository with lfs and other useful configurations"""
        path = pathlib.Path(self.repo_path)

        assert not next(path.rglob('*'), False), 'Selected dir %s is not empty' % self.repo_path

        path.mkdir(parents=True, exist_ok=True)

        self.git_repo = git.Repo.init(self.repo_path)
        self.git_repo.git.lfs('install')
        self.git_repo.git.config('core.sparseCheckout', 'true')

    def __setup_remote(self, remote_url, remote_name='origin'):
        """Adds a remote to the repo"""
        assert remote_name not in self.git_repo.remotes, 'Remote %s already exists' % remote_name

        remote = self.git_repo.create_remote(remote_name, remote_url)
        remote.fetch()

        return remote

    def __get_branch(self, branch_name, remote_name='origin'):
        """Gets a branch from a remote repository."""
        # TODO assert remote_name in self.git_repo.remotes, 'Remote %s does not exist' % remote_name

        remote = self.git_repo.remotes[remote_name]
        remote_ref = remote.refs[branch_name]
        head = self.git_repo.create_head(branch_name, remote_ref)
        head.set_tracking_branch(remote_ref)
        remote.fetch(depth=1)

    def __read_sparse_checkout(self):
        os.makedirs('.git/info', exist_ok=True)
        sparse_checkout = os.path.join(self.repo_path, '.git', 'info', 'sparse-checkout')

        if os.path.isfile(sparse_checkout):
            lines = [line.rstrip('\n') for line in open(sparse_checkout)]
            return sorted(set(filter(lambda pattern: pattern, lines)))
        else:
            return []

    def __write_sparse_checkout(self, patterns):
        sparse_checkout = os.path.join(self.repo_path, '.git', 'info', 'sparse-checkout')

        with open(sparse_checkout, 'w+') as out:
            out.write('\n'.join(set(patterns)))

    def __add_to_sparse_checkout(self, paths):
        patterns = self.__read_sparse_checkout()

        for path in paths:
            # TODO: Check if it's a file that it's not already included by another
            # pattern. If it's a directory remove any specific checkout for files or
            # directories that match. Assert that it does exist in the repo.
            patterns.append(path)

        self.__write_sparse_checkout(patterns)

    def __update_locks(self):
        logging.info('Updating locks...')
        self.git_repo.git.lfs('locks', '--json')

    def branches(self, remote_name='origin', remote_url=None):
        """List branches

        Returns a dict of branches from a remote repository
        """
        gitcmd = git.cmd.Git()
        if not remote_url:
            remote_url = self.git_repo.remotes[remote_name].url
        return {b[1][11:]: b[0] for b in (a.split('\t') for a in gitcmd.ls_remote('--heads', remote_url).splitlines())}

    def init(self):
        """Initializes an empty repository"""
        self.__create_empty()
        self.__add_to_sparse_checkout(GIT_FILES)

    def publish(self, remote_url):
        """Publish a repository to a remote url"""
        remote = self.__setup_remote(remote_url)
        self.git_repo.git.push(remote.name, '--all')

    def clone(self, remote_url, branch='master'):
        """Clone a repository from a remote url"""
        if not self.repo_path:
            path = urlparse(remote_url).path
            self.repo_path = os.path.abspath(re.findall(r'.*/([^/.]*)(?:\.git)?$', path)[0])
        self.__create_empty()
        self.__setup_remote(remote_url)
        self.__get_branch(branch)
        self.checkout(*GIT_FILES)

    def list(self, *paths, recursive=False):
        """List files in a repository"""
        for status in self.status(*paths, recursive=recursive, all=True):
            yield status.filename

    def status(self, *paths, recursive=True, all=False):
        """List status of the files in the working copy"""
        assert self.git_repo, 'No git repository defined here'

        if not paths:
            paths = ['.']

        statuses = collections.defaultdict(Status)

        for path in paths:
            path = display_name(path)

            # Get modified files
            for diff in self.git_repo.index.diff(None, path):
                filename = display_name(diff.b_path)
                if diff.new_file or diff.renamed_file or re.match(r'[MRCA]', diff.change_type):
                    statuses[filename].file_status = FileStatus.MODIFIED
                if diff.deleted_file or diff.change_type == 'D':
                    statuses[filename].file_status = FileStatus.DELETED

            # Get other status for all files in repo
            ls_tree_output = self.git_repo.git.ls_tree('HEAD', '--full-name', '--name-only', '-z',
                                                       '-r' if recursive else '-t', path)
            for filename in ls_tree_output.split('\0'):
                filename = display_name(filename)
                if filename and filename not in statuses and (all or filename == path):
                    if os.path.isfile(filename):
                        statuses[filename].file_status = FileStatus.UNMODIFIED
                    else:
                        statuses[filename].file_status = FileStatus.NOT_CHECKED_OUT

            # Get untracked files
            for filename in self.git_repo.untracked_files:
                filename = display_name(filename)
                if match_path(path, filename, recursive):
                    statuses[filename].file_status = FileStatus.UNTRACKED

            # Get locks status
            for lock_status in json.loads(self.git_repo.git.lfs('locks', '--json', '--cached')):
                filename = display_name(lock_status['path'])
                if filename in statuses:
                    statuses[filename].lock_status = LockStatus.LOCKED_OTHER

            for lock_status in json.loads(self.git_repo.git.lfs('locks', '--json', '--local')):
                filename = display_name(lock_status['path'])
                statuses[filename].lock_status = LockStatus.LOCKED

        # Yield all the statuses to the caller
        for filename in statuses:
            statuses[filename].filename = filename
            yield statuses[filename]

    def checkouts(self):
        """List checkout patterns"""
        logging.info('Current checkouts:')
        logging.info('')
        for pattern in self.__read_sparse_checkout():
            yield pattern

    def checkout(self, *paths, lock=False):
        """Checkout files or directories from the repository"""
        logging.info('Getting files from server...')
        self.__add_to_sparse_checkout(paths)
        self.git_repo.git.read_tree('-mu', 'HEAD')

        if lock:
            self.__update_locks()
            self.lock(*paths)
        logging.info('Done checking out files')

    def uncheckout(self, *paths):
        """Remove files from the working copy but keep them on the repository"""
        patterns = self.__read_sparse_checkout()

        for path in paths:
            assert path in patterns, 'Path %s not in checked out patterns'
            patterns.remove(path)

        self.__write_sparse_checkout(patterns)

        self.git_repo.git.read_tree('-mu', 'HEAD')

    def checkin(self, message, *paths, keep_lock=False):
        """Check-in changes to the remote repository"""
        if paths:
            # TODO remember state index to recreate afterwards
            print('paths=%s' % paths)
            self.git_repo.git.reset()
            self.git_repo.git.add(*paths)
        else:
            paths = ['.']
            self.git_repo.git.add('-u')

        self.git_repo.git.commit('-m', message)
        self.git_repo.git.push()
        # TODO git "undo" if not successful

        if not keep_lock:
            unlock_paths = []
            for path in paths:
                for lock_status in json.loads(self.git_repo.git.lfs('locks', '--json', '--local')):
                    filename = display_name(lock_status['path'])
                    if match_path(path, filename):
                        unlock_paths.append(filename)
            if unlock_paths:
                self.unlock(*unlock_paths)

    def update(self):
        """Get new changes from the remote repository"""
        for remote in self.git_repo.remotes:
            logging.info('Updating from remote %s...' % remote.name)
            remote.pull()
        self.__update_locks()
        logging.info('Done updating working copy')

    def lock(self, *paths):
        """Lock a file or directory"""
        self.git_repo.git.lfs('lock', *paths)
        self.__update_locks()

    def unlock(self, *paths):
        """Unlock a file or directory"""
        self.git_repo.git.lfs('unlock', *paths)
        self.__update_locks()

    def compact(self):
        """Reduce the repository size"""
        logging.info('Pruning large file cache...')
        self.git_repo.git.lfs('prune')
        logging.info('Removing old revisions...')
        for remote in self.git_repo.remotes:
            remote.pull(depth=1)
        self.git_repo.git.gc(prune='all')
        logging.info('Done compacting working copy')
