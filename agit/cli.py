# encoding: UTF-8

import argparse
import collections
import logging
import os
import re
import sys
import textwrap

import colorama
from colorama import Fore, Back, Style

from .agitrepo import AGitRepo, FileStatus, LockStatus, display_name

FILE_STATUS_COLORS = {FileStatus.UNMODIFIED: Fore.RESET + Style.NORMAL,
                      FileStatus.MODIFIED: Fore.YELLOW + Style.NORMAL,
                      FileStatus.DELETED: Fore.RED + Style.NORMAL,
                      FileStatus.UNTRACKED: Fore.WHITE + Style.NORMAL,
                      FileStatus.NOT_CHECKED_OUT: Fore.LIGHTBLACK_EX + Style.DIM}

LOCK_STATUS_COLORS = {LockStatus.UNLOCKED: Fore.LIGHTBLACK_EX + Style.DIM,
                      LockStatus.LOCKED: Fore.GREEN + Style.NORMAL,
                      LockStatus.LOCKED_OTHER: Fore.RED + Style.NORMAL}


def print_files(files):
    # TODO read colors from LS_COLORS
    for f in files:
        color = Style.BRIGHT + Fore.BLUE
        if os.path.isdir(f):
            print(color + display_name(f))
        else:
            file_style = Style.RESET_ALL + (Style.BRIGHT if os.path.isfile(f) else Style.DIM)
            print(re.sub(r'(.*/)?([^/]*)', r'%s\1%s\2' % (color, file_style), display_name(f)))


def print_result(result):
    if isinstance(result, collections.abc.Iterable):
        for item in result:
            print(item)
    elif result:
        print(result)


def print_checkouts(result):
    for checkout in result:
        type = Fore.YELLOW + 'R' if os.path.isdir(checkout) else Fore.GREEN + 'F'
        print('%s%s  %s' % (type, Style.RESET_ALL, checkout))


def print_status(result):
    for status in result:
        print('%s%s%s  %s' % (LOCK_STATUS_COLORS[status.lock_status] + status.lock_status.value,
                              FILE_STATUS_COLORS[status.file_status] + status.file_status.value,
                              Style.RESET_ALL,
                              status.filename))


def __add_parser(subparsers, func, *args, **kwargs):
    doclines = func.__doc__.splitlines()
    help_ = doclines[0]

    if len(doclines) > 1:
        description = '%s\n%s\n%s' % (help_, '=' * len(help_), textwrap.dedent('\n'.join(doclines[1:])))
    else:
        description = help_

    subparser = subparsers.add_parser(*args,
                                      help=help_,
                                      description=description,
                                      formatter_class=argparse.RawTextHelpFormatter,
                                      **kwargs)
    subparser.set_defaults(func=func)

    return subparser


def setup_parser():
    parser = argparse.ArgumentParser(description=AGitRepo.__doc__)
    parser.add_argument('--quiet', '-q', action='store_true', help='Show less output')
    parser.add_argument('--color', choices=['always', 'auto', 'never'], default='auto', help='Show colored output')
    subparsers = parser.add_subparsers(title='Commands', metavar='', required=True)

    # Branches
    subparser = __add_parser(subparsers, AGitRepo.branches, 'branches', aliases=['b'])
    subparser.add_argument('remote_url', nargs='?', help='Remote URL')
    subparser.add_argument('--remote_name', nargs='?', default='origin', help='Remote Name')

    # Init
    subparser = __add_parser(subparsers, AGitRepo.init, 'init', aliases=['i'])
    subparser.add_argument('repo_path', default='.', help='Repository path')

    # Clone
    subparser = __add_parser(subparsers, AGitRepo.clone, 'clone', aliases=['c'])
    subparser.add_argument('remote_url', default='.', help='Remote URL')
    subparser.add_argument('repo_path', default=None, help='Repository path')

    # List
    subparser = __add_parser(subparsers, AGitRepo.list, 'list', aliases=['ls'])
    subparser.add_argument('paths', nargs='*', default=['.'], help='Limit to the provided paths')
    subparser.add_argument('--recursive', '-r', action='store_true', help='Recurse into subdirectories')
    subparser.set_defaults(printer=print_files)

    subparser = __add_parser(subparsers, AGitRepo.status, 'status', aliases=['st'])
    subparser.add_argument('paths', nargs='*', default=['.'], help='Limit to the provided paths')
    subparser.add_argument('--all', '-a', action='store_true', help='Output all files from repo')
    subparser.add_argument('--non-recursive', '-R', action='store_false', dest='recursive',
                           help='Do not show sub-directories')
    subparser.set_defaults(printer=print_status)

    subparser = __add_parser(subparsers, AGitRepo.checkouts, 'checkouts', aliases=['cos'])
    subparser.set_defaults(printer=print_checkouts)

    subparser = __add_parser(subparsers, AGitRepo.checkout, 'checkout', aliases=['co'])
    subparser.add_argument('paths', nargs='+', help='File or directory paths')
    subparser.add_argument('--lock', action='store_true', help='Lock file after check-out')

    subparser = __add_parser(subparsers, AGitRepo.uncheckout, 'uncheckout', aliases=['uco'])
    subparser.add_argument('paths', nargs='+', help='File or directory path')

    subparser = __add_parser(subparsers, AGitRepo.checkin, 'checkin', aliases=['ci'])
    subparser.add_argument('paths', default='.', help='File or directory paths')
    subparser.add_argument('--message', '-m', help='Check-in message')

    subparser = __add_parser(subparsers, AGitRepo.update, 'update', aliases=['up'])

    subparser = __add_parser(subparsers, AGitRepo.lock, 'lock')
    subparser.add_argument('paths', help='File or directory path')

    subparser = __add_parser(subparsers, AGitRepo.unlock, 'unlock')
    subparser.add_argument('paths', help='File or directory path')

    subparser = __add_parser(subparsers, AGitRepo.compact, 'compact')

    return parser


def main():
    parser = setup_parser()

    args = vars(parser.parse_args())

    color = args.pop('color')
    colorama.init(autoreset=True, strip=color == 'never' or (not sys.stdout.isatty() and color != 'always'))
    logging.basicConfig(format='', level=logging.WARN if args.pop('quiet', False) else logging.INFO)

    func = args.pop('func')
    printer = args.pop('printer', print_result)
    repo_path = args.pop('repo_path', '.')

    try:
        a = AGitRepo(repo_path)
        printer(func(a, **args))
    except AssertionError as e:
        logging.error(e)
