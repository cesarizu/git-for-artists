import tempfile
import os
import pytest
import uuid

import git

from agit.agitrepo import AGitRepo, FileStatus, LockStatus, match_path


def write_to_file(file, contents):
    with open(file, 'a') as out_file:
        out_file.write(contents)


def add_file_to_git_repo(repo, file, contents):
    write_to_file(file, contents)
    repo.index.add([file])
    repo.index.commit('Added ' + file)


def setup_git_repo(repo):
    repo.git.config('user.email', 'test@example.com')
    repo.git.config('user.name', 'Test Name')

    if not pytest.config.getvalue("skip-lfs"):
        repo.git.config('lfs.url', 'https://testadmin:testpass@localhost:8080/test/%s' % uuid.uuid4())
        repo.git.config('http.sslVerify', 'false')


@pytest.fixture
def empty_remote_repo():
    with tempfile.TemporaryDirectory() as bare_repo_path:
        os.chdir(bare_repo_path)
        bare_repo = git.Repo.init('.', bare=True)
        yield bare_repo


@pytest.fixture
def empty_remote_repo_url(empty_remote_repo):
    return 'file://' + empty_remote_repo.common_dir


@pytest.fixture
def remote_repo():
    with tempfile.TemporaryDirectory() as bare_repo_path, tempfile.TemporaryDirectory() as repo_path:
        os.chdir(bare_repo_path)
        bare_repo = git.Repo.init('.', bare=True)

        os.chdir(repo_path)
        repo = git.Repo.init('.')

        setup_git_repo(repo)

        add_file_to_git_repo(repo, '.gitattributes', '*.bin filter=lfs diff=lfs merge=lfs -text\n')
        add_file_to_git_repo(repo, '.gitignore', '')
        add_file_to_git_repo(repo, 'test.bin', 'binary content')
        add_file_to_git_repo(repo, 'test.txt', 'text content')

        origin = repo.create_remote('origin', 'file://' + bare_repo_path)
        origin.push('master:master')

        yield bare_repo


@pytest.fixture
def remote_repo_url(remote_repo):
    return 'file://%s' % remote_repo.common_dir


@pytest.fixture
def cloned_repo(remote_repo_url):
    with tempfile.TemporaryDirectory() as working_copy_path:
        os.chdir(working_copy_path)
        repo = AGitRepo('.')
        repo.clone(remote_repo_url)

        setup_git_repo(repo.git_repo)

        yield repo


@pytest.fixture
def new_repo():
    with tempfile.TemporaryDirectory() as working_copy_path:
        os.chdir(working_copy_path)
        repo = AGitRepo('.')
        repo.init()

        setup_git_repo(repo.git_repo)

        add_file_to_git_repo(repo.git_repo, '.gitattributes', '*.bin filter=lfs diff=lfs merge=lfs -text\n')
        add_file_to_git_repo(repo.git_repo, '.gitignore', '')
        add_file_to_git_repo(repo.git_repo, 'test.bin', 'binary content')
        add_file_to_git_repo(repo.git_repo, 'test.txt', 'text content')

        yield repo


def number_of_commits(repo):
    return len(list(repo.git_repo.iter_commits()))


def test_match_path():
    # Match all simple file
    assert match_path('', 'test.txt')
    assert match_path('.', 'test.txt')
    assert match_path('./', 'test.txt')

    # Match all nested file
    assert match_path('', 'subdir/test.txt')
    assert match_path('.', 'subdir/test.txt')
    assert match_path('./', 'subdir/test.txt')

    # Match nested file
    assert not match_path('subdir/', 'test.txt')
    assert not match_path('./subdir/', 'test.txt')
    assert match_path('subdir/', 'subdir/test.txt')
    assert match_path('./subdir/', 'subdir/test.txt')

    # Match recursive nested file
    assert not match_path('subdir/', 'test.txt')
    assert match_path('subdir/', 'subdir/test.txt')
    assert match_path('subdir/', 'subdir/more/test.txt')
    assert not match_path('subdir/', 'other/test.txt')

    # Match non recursive nested file
    assert not match_path('subdir/', 'test.txt', recursive=False)
    assert match_path('subdir/', 'subdir/test.txt', recursive=False)
    assert not match_path('subdir/', 'subdir/more/test.txt', recursive=False)
    assert not match_path('subdir/', 'other/test.txt', recursive=False)

    # Match exact file
    assert not match_path('test.txt', 'test.bin')
    assert not match_path('test.txt', 'test.txt.bak')
    assert match_path('test.txt', 'test.txt')


def test_remote_branches(remote_repo_url):
    remote_branches = AGitRepo().branches(remote_url=remote_repo_url)
    assert len(remote_branches) == 1
    assert 'master' in remote_branches


def test_branches(cloned_repo):
    remote_branches = cloned_repo.branches()
    assert len(remote_branches) == 1
    assert 'master' in remote_branches


def test_init():
    with tempfile.TemporaryDirectory() as repo_path:
        os.chdir(repo_path)
        repo = AGitRepo('.')
        repo.init()
        assert repo.git_repo is not None


def test_publish(new_repo, empty_remote_repo_url):
    new_repo.publish(empty_remote_repo_url)

    with tempfile.TemporaryDirectory() as working_copy_path:
        os.chdir(working_copy_path)
        repo = AGitRepo()
        repo.clone(empty_remote_repo_url)
        os.chdir(repo.repo_path)

        files = sorted(repo.list())
        assert len(files) == 4


def test_clone(cloned_repo):
    assert cloned_repo.git_repo is not None


def test_list(cloned_repo):
    files = sorted(cloned_repo.list())
    assert len(files) == 4
    assert files[0] == '.gitattributes'
    assert files[1] == '.gitignore'
    assert files[2] == 'test.bin'
    assert files[3] == 'test.txt'

    # Test paths filtering
    files = sorted(cloned_repo.list('test.bin', 'test.txt'))
    assert len(files) == 2

    # TODO test recursive


def test_status(cloned_repo):
    statuses = sorted(cloned_repo.status())
    assert statuses == []

    # Test all=True
    statuses = sorted(cloned_repo.status(all=True))
    assert len(statuses) == 4
    assert statuses[0].file_status == FileStatus.UNMODIFIED
    assert statuses[1].file_status == FileStatus.UNMODIFIED
    assert statuses[2].file_status == FileStatus.NOT_CHECKED_OUT
    assert statuses[3].file_status == FileStatus.NOT_CHECKED_OUT

    cloned_repo.checkout('test.txt')

    # Test paths filtering
    statuses = sorted(cloned_repo.status('test.bin', 'test.txt'))
    assert len(statuses) == 2

    # Test all FileStatus values
    write_to_file('.gitignore', 'ignored\n')
    os.remove('test.txt')
    write_to_file('untracked', 'untracked')
    write_to_file('ignored', 'ignored')

    statuses = sorted(cloned_repo.status(all=True))
    assert len(statuses) == 5
    assert statuses[0].file_status == FileStatus.UNMODIFIED
    assert statuses[1].file_status == FileStatus.MODIFIED
    assert statuses[2].file_status == FileStatus.NOT_CHECKED_OUT
    assert statuses[3].file_status == FileStatus.DELETED
    assert statuses[4].file_status == FileStatus.UNTRACKED

    # TODO test recursive


def test_checkouts(cloned_repo):
    checkouts = sorted(cloned_repo.checkouts())
    assert len(checkouts) == 2
    assert checkouts[0] == '.gitattributes'
    assert checkouts[1] == '.gitignore'


def test_checkout(cloned_repo):
    cloned_repo.checkout('test.bin')

    checkouts = sorted(cloned_repo.checkouts())
    assert len(checkouts) == 3
    assert checkouts[2] == 'test.bin'
    assert os.path.isfile('test.bin')


@pytest.mark.lfs
def test_checkout_with_lock(cloned_repo):
    cloned_repo.checkout('test.txt', lock=True)

    statuses = sorted(cloned_repo.status('test.txt'))
    assert statuses[0].lock_status == LockStatus.LOCKED


def test_uncheckout(cloned_repo):
    cloned_repo.checkout('test.txt')
    cloned_repo.uncheckout('test.txt')

    checkouts = sorted(cloned_repo.checkouts())
    assert len(checkouts) == 2
    assert not os.path.isfile('test.txt')


def test_checkin(cloned_repo):
    cloned_repo.checkout('test.txt')

    previous_commits = number_of_commits(cloned_repo)
    write_to_file('test.txt', 'more content')
    cloned_repo.checkin('add more content')

    assert number_of_commits(cloned_repo) == previous_commits + 1

    write_to_file('new_file', 'new file')
    cloned_repo.checkin('add new_file', 'new_file')

    statuses = sorted(cloned_repo.status('new_file'))
    assert statuses[0].file_status == FileStatus.UNMODIFIED
    assert number_of_commits(cloned_repo) == previous_commits + 2


@pytest.mark.lfs
def test_checkin_no_keep_lock(cloned_repo):
    cloned_repo.checkout('test.txt', lock=True)
    write_to_file('test.txt', 'more content')
    cloned_repo.checkin('do not keep lock')

    statuses = sorted(cloned_repo.status('test.txt'))
    assert statuses[0].lock_status == LockStatus.UNLOCKED


@pytest.mark.lfs
def test_checkin_keep_lock(cloned_repo):
    cloned_repo.checkout('test.txt', lock=True)
    write_to_file('test.txt', 'more content')
    cloned_repo.checkin('keep lock', keep_lock=True)

    statuses = sorted(cloned_repo.status('test.txt'))
    assert statuses[0].lock_status == LockStatus.LOCKED


def test_update(cloned_repo):
    # TODO create commit in remote repo
    cloned_repo.update()
    # TODO assert it is updated


@pytest.mark.lfs
def test_lock(cloned_repo):
    statuses = sorted(cloned_repo.status(all=True))
    assert statuses[0].lock_status == LockStatus.UNLOCKED

    cloned_repo.checkout('test.txt', lock=True)

    statuses = sorted(cloned_repo.status('test.txt'))
    assert statuses[0].lock_status == LockStatus.LOCKED


@pytest.mark.lfs
@pytest.mark.skip(reason='test not implemented yet')
def test_lock_other(cloned_repo):
    # TODO lock test.txt with other user

    statuses = sorted(cloned_repo.status('test.txt'))
    assert statuses[0].lock_status == LockStatus.LOCKED


@pytest.mark.lfs
def test_unlock(cloned_repo):
    cloned_repo.checkout('test.txt', lock=True)

    statuses = sorted(cloned_repo.status('test.txt'))
    assert statuses[0].lock_status == LockStatus.LOCKED

    cloned_repo.unlock('test.txt')

    statuses = sorted(cloned_repo.status('test.txt'))
    assert statuses[0].lock_status == LockStatus.UNLOCKED


def test_compact(cloned_repo):
    # TODO Create revisions

    cloned_repo.compact()

    # TODO assert the repo is shallow
