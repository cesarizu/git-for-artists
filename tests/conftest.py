import pytest


def pytest_addoption(parser):
    parser.addoption("--skip-lfs", dest="skip-lfs", action="store_true", default=False,
                     help="skip tests that require a lfs server")


def pytest_collection_modifyitems(config, items):
    if not config.getoption("--skip-lfs"):
        return

    skip_lfs = pytest.mark.skip(reason="Used --skip-lfs option")
    for item in items:
        if "lfs" in item.keywords:
            item.add_marker(skip_lfs)
