# Git For Artists

An easy way of using git for asset-heavy repositories with a simplified workflow.

Git for artists, or agit for short, is a command line utility that makes it easy and fast to work with large git
repositories. It uses git under the hood, so you can still use any git compatible client or server to host your files.
This is a client side only tool that reduces the time needed to clone and work with git repositories, specially ones
that have large files. 

##### This is a work in progress

## Features:

- Simplified workflow
- Minimizes working copy size by using:
  - Git LFS
  - Shallow clones
  - Sparse checkouts
- Fully compatible with git, does not aim to replace it. Works as a complementary tool. 

## TO-DO

See https://gitlab.com/cesarizu/git-for-artists/issues for an up-to-date list of things to do.

## Workflow

```
    +---------------------+
    |  Remote Repository  |
    +--+------------------+
       |              ^
       | clone        |
       |              |
       v              |
    +---------------------+
    |  Local Repository   |
    +--+------------------+
       |               |
       | check-out     | check-in
       |               |
       v               |
    +------------------+--+
    |    Working Copy     |
    +---------------------+
```

* **Remote Repository**: Can be any git repository that has LFS support.
* **Local Repository**: Is the local copy of the repository.
* **Working Copy**: Your local files.

### Command line usage

To **clone** an existing repository:

```bash
agit clone URL
```

This will create an empty working copy by default. To get any files you need to check them out.

**List available files** in the repository:

```bash
agit list [PATH]
```

**Checking out a file** copies it to the working copy.

```bash
agit checkout PATH [PATH...]
```

**Locking a file** prevents others to modify it and can be done like this:

```bash
agit lock PATH [PATH...]
```

Check the **status of your working copy** to see what changes have you made and what files will be checked in:

```bash
agit status
```

**Check-in** your changes to upload them to the server:

```bash
agit checkin "Text describing what was changed"
```

**Get new changes** from the server:

```bash
agit update
```

To see what else you can do, or to get **help**, run:

```bash
agit --help
```

## Installation

### Pre-requisites

* [Git](https://git-scm.com/)
* [Git LFS](https://git-lfs.github.com/)

For **Windows** you can install [Git For Windows](https://git-scm.com/download/win). The installer has LFS
support built in.

On **MacOS** install with the [Git OS X installer](https://git-scm.com/download/mac) and install Git LFS from the
[Git LFS website](https://git-lfs.github.com/). You can also install them via [Homebrew](brew.sh).

```
brew install git git-lfs
```

On **Linux** use your package manager to install Git and Git LFS or visit the
[Git LFS website](https://git-lfs.github.com/) for more details and options.

### Development

There's currently no installer for this project. To use it you need to install some dependencies:

* [Python 3](https://www.python.org/)
* [Poetry](https://poetry.eustace.io/)
 
Then clone this repository and run as follows:

```bash
git clone https://gitlab.com/cesarizu/git-for-artists
cd git-for-artists
poetry shell
```

The `agit` command will be available on the path to be run. 

### Testing

Tests be optionally run with a lfs server to test locking functionality and other large file support. For this you can
use [GitHub's test lfs server](https://github.com/github/lfs-test-server). There are scripts on the `bin` directory
that help setup and run the test server. This requires that `go` be installed.

To download and setup the test lfs server run: 
 
```bash
bin/setup-lfs-test-server
```

To test this project, run the following commands:

```bash
bin/run-lfs-test-server &
poetry run pytest
```

To run without the test lfs server run:

```bash
poetry run pytest --no-lfs
```

See `.gitlab-ci.yml` to see how this project is tested in GitLab CI.

## F.A.Q.

**Could `agit` support feature X?**

Maybe? But you can always run `git` for any advanced use. The main idea is to cover the most common and simple cases
that a non-software developer might need.

**Can I use it on existing repositories?**

Yes. You can run `agit compact` to make the repository shallow and enable sparse checkouts.

## Author

Copyright by César Izurieta. Released under the MIT License. See the COPYING file for more details.